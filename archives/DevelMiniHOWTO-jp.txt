----------------------------------------------------------------------------

KDE 開発者のためのミニ・HOWTO (1999年5月21日)
				著者 David Sweet <dsweet@chaos.umd.edu>
				日本語訳 高木淳司 <j-takagi@ccs.mt.nec.co.jp>

I. はじめに

KDE あるいは KDE アプリケーションの開発を支援するためには、(i) KDE の情報と
コードの探し方、(ii) CVS の使い方、(iii)コンパイルのやり方、について知る必要が
あります。この文書はあなたがあまり腹を立てることなしにこれらのことをするのを
助けることを目的としています。

このミニ・HOWTO は新参の開発者、時たまの開発者、そして HEAD ブランチを動かす
ことができなさそうな人達に合わせて書かれています。ここでは最近私や他の人達が
kde-devel メーリングリストに質問したことに加えていくつかの一般的な情報について
述べています。

この文書の最新版は http://www.chaos.umd.edu/~dsweet/KDE/DevelMiniHOWTO.txt に
あります。

貢献、コメント、訂正などは私 (dsweet@chaos.umd.edu) に e-mail を下さい。

II. 情報とソースコード

一般的な情報

Kデスクトップ環境は世界中のボランティアによって作り上げられたオープンソースの
Unix デスクトップです。このプロジェクトに貢献する方法はいくつもあります。
新しいコードを書く、古いコードを改善する、文書を書く、他の言語に翻訳する、
絵や音や音楽を創る、バグを報告する、新しい特徴を提案する、などです。
もしあなたがコードの開発を行いたいと考えているならこのまま読み進めて下さい。
もしそれ以外の方法で貢献したいと考えているなら KDE ウェブサイト(www.kde.org)を
訪ねて情報を得てください。

KDE は Troll Tech (www.troll.no) によって開発された Qt ツールキットを使用して
います。デスクトップの主要なコンポーネントはファイルマネージャ (kfm)、
ウィンドウマネージャ (kwm)、そしてパネル (kpanel) です。それ以外にも多くの
ユーティリティやアプリケーションが基本ディストリビューションに含まれています。

開発に使われている主要なプログラミング言語は C++ ですが、Python バインディング
(pyKDE)も利用することができます。KDE のコードは様々な目的のために作られた
クラスを含むライブラリから成っています。例えば、[libkdecore] アプリケーションの
ベース (KApplication)、コンフィギュレーションファイルへのアクセス (KConfig)、
外部プロセスをの起動 (KProcess)、[libkdeui]] ウィジェット (KEdit, KFontDialog,
KToolBar など) があります。その他に KFileDialog (ファイルダイアログ)、KSpell
(スペルチェッカー) のようなユーティリティクラスもあります。様々なデスクトップ、
設定、システム管理ユーティリティもディストリビューションに含まれています。
それらは KDE の開発者が取り組んでいるものです。より新しいものとしては
KOffice というワードプロセッサ、スプレッドシート、プレゼンテーションツールを
含む生産的なツールのセットがあります。

現在の正式にリリースされた KDE のバージョンは 1.1.1 です。KDE 1.1.2 には
テーマのサポート(ユーザがデスクトップの見栄えを変えることを可能にします)と
バグ修正が入るでしょう。これはく4-6ヵ月のうちにリリースされる見込みです。
恐らく今現在の開発の焦点となっている KDE 2.0 には kfm, khtmlw (HTML
ウィジェット), kpanel, kmain, 多分 kwm も、そしてそれ以外にもたくさんの
コードの書き直しあるいは大きな変更が入るでしょう。

KDE 2.0 にはフリーの CORBA の実装である mico が必要です。CORBA は新しい kfm
(Konquerer と呼ばれています) を容易に拡張できるように、それから、KOffice で
ドキュメント中心のユーザインターフェースを構築するために使われます。ユーザは、
例えば、KSpread で作られて編集された図表を KWord で作られて編集された文書に
貼り込むことができます。ここに Simon Hausmann が書いた KOM/OpenParts に関する
説明を載せます。

  OpenParts は KOM に基づいています。OpenParts は、簡単に他のアプリケーションの "ウィジェット" をグラフィカルに張り込んだり、GUI の要素を賢いやりかたで共有管理したりする機能を提供します(together with a CORBA/KOM interface/implementation of these (?))。
  OpenParts はドキュメント - ビューモデルの基本的なサポートを実装しており、
  KOffice によって使われています。

より詳しくは CVS の中の kdenonbeta/corbadoc/komop.html を参照して下さい。

URL

KDE のメインのウェブサイト : http://www.kde.org
  ナビゲーションサイドバーの "Developer information" のところをご覧下さい。
バグレポート : http://bugs.kde.org
Troll Tech (Qt ツールキットの開発者) のメインのウェブサイト : http://www.troll.no

KDE のソースとバイナリ : ftp://ftp.kde.org (またはそのミラー)
Qt  のソースとバイナリ : ftp://ftp.troll.no

CORBA の情報 : http://www.omg.org/
CORBA の論文 : http://www.cs.wustl.edu/~schmidt/vinoski.ps.gz
MICO : http://diamant-atm.vsb.cs.uni-frankfurt.de/~mico/ (ドキュメントをご覧下さい! よくできています。)
OpenParts/KOM : http://www.chaos.umd.edu/~dsweet/KDE/KTrans/KOM

メーリングリスト

kde-devel メーリングリストは一般的な KDE 開発者のためのものです。
koffice メーリングリストは KOffice の開発に興味を持っている開発者のためのものです。
そして、kfm-devel メーリングリストは kfm (ファイルマネージャ)の開発者のためのものです。

kde-devel-request か koffice-request に "subscribe myid@myserver" (myid@myserver はあなたの e-mail アドレスを表します)というメッセージを送って下さい。
KDE mail ページの "Mailing Lists" の所をクリックすると他のメーリングリストやメーリングリストのアーカイブなどの情報を得ることができます。

Troll Tech は Qt-2.0 のスナップショット(詳しくは後で述べます)のユーザのためのメーリングリストを提供しています。メールの本文に "subscribe" と書いたメールを snapshot-users-request@troll.no に送ることで登録することができます。
(訳注：詳しくは http://www.troll.no/dl/qtfree-cvs.html をご覧下さい。)

KDE 開発者として joedeveloper@kde.org のような e-mail アドレスが欲しいか又は
必要になるかもしれません。その場合は Martin Konold (konold@kde.org) に
e-mail で連絡して下さい。

KDE の CVS リポジトリ(後で詳しく述べます)へのアクセス権を得るには暗号化された
パスワードを Sutephen Kulow (coolo@kde.org) に e-mail で送って下さい。

あなた自身の暗号化されたパスワードを得るには次のようにして下さい。
	perl -e print\ crypt\('passwd','sa'\)\.\"\\n\"
passwd はあなたのパスワードで、sa は [a-zA-Z0-0./] の中の適当な二つの文字です。
その出力があなたの暗号化されたパスワードです。

II. KDE CVS

KDE CVS (Concurrent Versions System) は KDE プロジェクトのソースコード
リポジトリです。それには (i) WWW: http://www.、(ii) cvs ユーティリティ、
(iii) cvsup ユーティリティ、(iv) スナップショット といった手段でアクセス
できます。(ii) だけには CVS のアクセス権が必要です。(i), (iii), (iv) は
読み出し専用の手段で、誰でも使うことができます。

(i) の方法のウェブページにはその使い方が説明してあります。私は (iii) の
cvsup の使い方はまだ知りません。http://www.kde.org/cvsup.html をご覧下さい。
[-- たぶん、既にそれを使っている誰かが簡単な使い方とその情報の入手方法を
コントリビュートしてくれるでしょう。 --]
(iv) のスナップショットは、KDE のコードのある部分(モジュールと呼ばれています。
例えば kdelibs や kdeutils のようなものがあります)を含んだ .tar.bz2 ファイル
で、ある特定の日付を(ファイルネーム中に、例えば kdelibs990517.tar.bz2 のように)
示しています。スナップショットは毎日
ftp://ftp.kde.org/pub/kde/unstable/CVS/snapshots にポストされます。
[(ii) については後で述べます。]

リポジトリ (あるいは単に CVS) は変更点を undo できるように全ての貢献者によって
なされたソースコードへの変更を全て蓄積しています。ユーザは変更を行うたびに
コードの開発を追いかけるのを簡単にするためにコメントを含めます。それらの
コメントは kde-cvs メーリングリストに送られます。CVS は KDE プロジェクトの
異なるバージョンを維持するために "ブランチ(枝)" に分かれます。例えば、
今開発が行われている二つのブランチは KDE_1_1_BRANCH と HEAD です。それらは
両方とも同じコードから進化したものです(つまり、もしそれぞれのブランチに
なされた変更を十分なだけ undo すれば全く同一のソースコードになります)が、
異なる目的に使用されています。ここにいくつかのブランチとその説明を参考のために
載せておきます。
    KDE_1_1_1_RELEASE - 安定しているとみなされ、KDE 1.1.1 としてリリースされ
                        配布されているコードです。
    KDE_1_1_BRANCH - KDE_1_1_1_RELEASE に源を発し、 KDE 1.1.2 に向けて開発が
                     行われています。テーマのサポートとバグ修正のみが入ります。
                     Qt 1.42 をベースにしています。
    HEAD - KDE 2.0 となるコードです。Qt 2.0 をベースにしており、mico が必要で、
           新しい kwm, kpanel などなど、そして*多くの*変更がなされています。

cvs ユーティリティは多分あなたのシステムにあるでしょう。その使い方を知るには
man ページを読むことをお勧めします。ここでは、いくつかの共通の機能の解説を
述べます。

仮にユーザ名を joedeveloper としましょう。以下に書かれているように全ての
コマンドをどこかのベースディレクトリで入力して下さい。
(KDE/CVS は悪くない選択です!)

CVSROOT 環境変数を :pserver:joedeveloper@cvs.kde.org:/home/kde にセットします。

モジュールをチェックアウトするには、例えば kdelibs を HEAD ブランチから
取り出すには次のようにします。
	cvs -z6 checkout -r HEAD kdelibs
	cvs -z6 checkout kdelibs

-z6 オプションは、サーバに対してコードを送る前に "レベル6" で圧縮するように
指示します。これでスピードアップをはかることができます。-r オプションは cvs に
どのブランチからチェックアウトしたいかを指示します。何も指定しなかった場合の
デフォルトは HEAD ブランチです。

例えば kdelibs を KDE_1_1_BRANCH ブランチから取り出すには次のようにします。
	cvs -z6 checkout -r KDE_1_1_BRANCH kdelibs

注: checkout の省略形として co を使うこともできます。

モジュールの中からアプリケーションをチェックアウトする(例えば kdeutils
モジュールの中の kjots とか)場合、HEAD ブランチから取り出すには次のように
します。
(1)	cvs -z6 co -l kdeutils
(2)	cvs -z6 co -l admin
(3)	cvs -z6 co -l kdeutils/kjots
(4)	cd kdeutils; ln -s ../admin

(1) の -l はcvs に kdeutils のサブディレクトリを再帰的には取り出さないように
指示します。つまり、コンフィギュレーションスクリプトとその仲間(後で述べます)
だけを取り出し、アプリケーションのソースコードは取り出さないことになります。

(2) は autoconf とその仲間を含む admin ディレクトリを取り出します。
(このディレクトリはモジュールをまるごとチェックアウトする場合は自動的に
取り出されます。)

(3) は kjots のソースを取り出します。

(4) は admin ディレクトリへのシンボリックリンクを張ります。(これは
ディレクトリをコピーしたり移動したりするよりも良い方法です。
admin ディレクトリを cvs で取り出したままの場所に置いておけば、cvs で
簡単にアップデートすることができます。さらにチェックアウトした他の
モジュールからシンボリックリンクを張ることもでき、そうすることによって
ただ一つの最も新しい admin のコピーを持つことができるのです。)

以前にチェックアウトしたコードをアップデートするには(例えば kdeutils/kjots の
場合)次のようにします。
	cvs -z6 update kdeutils/kjots

あなたのハードディスク上の kjots のソースコードは CVS のコードと合うように
アップデートされます。ここではブランチを指定する必要はありません。
kdeutils/kjots/CVS/Tag にブランチが保存されているのです。

修正をコミットする(CVS に書き込む)には(例えば kdeutils/kjots の場合)
次のようにします。
	cvs -z6 commit kdeutils/jots
すると、コメントを編集するプロンプトが出るでしょう。そのコミットで行おうと
している変更について簡潔に入力して下さい。(EDITOR または CVSEDITOR 環境変数を
セットすることで好みのエディタを使うことができます)

ファイルを追加するには (例えば kdeutils/kmyapp/greatnewcode.cpp の場合)
次のようにします。
	[まずはファイルを生成してください!]
	cd kdeutils/kmyapp
	cvs add greatnewcode.cpp
	cvs commit

ファイルを削除するには (例えば kdeutils/kmyapp/badoldcode.cpp の場合)
次のようにします。
	cd kdeutils/kmyapp
	rm badoldcode.cpp
	cvs remove badoldcode.cpp
	cvs commit

ディレクトリ(モジュール、アプリなど)を追加するには (例えば kdeutils/kmyapp に
ソースファイルとして kmysource.cpp を) 次のようにします。
	cd kdeutils
	mkdir kmyapp
	(kmyapp/kmysource.cpp ファイルを生成します)
	cvs add kmyapp
	cvs add kmyapp/kmysource.cpp
	cvs commit    (実際にディレクトリとファイルを CVS に置きます)

注: コミットするにはディレクトリ内にファイルがなければなりません。

ディレクトリ(モジュール、アプリなど)を削除するには (例えば kdeutils/kmyapp の
場合) 次のようにします。
	cd kdeutils/kmyapp
	(上記のファイルを削除する方法にしたがって全てのファイルを削除して下さい)
	cd ..
	cvs -P update  (will remove the local kmyapp automatically)


III. コンパイルと開発の実際 (configure を用いて)

最新のソースをダウンロードしてコンパイルする前に、それは動かないかもしれないと
いうことを十分に認識しておいて下さい! 最新のソースは常に開発中の状態にあるため
頻繁にバグに遭遇します。

それを知った上で、既にあなたのマシンに存在する安定した KDE の設定に影響を
与えずに新しい KDE をコンパイルして実行する方法を見つけなければなりません。
ここでは例として HEAD ブランチを使って一つの方法を紹介します。ここでは再度
ログイン名を joedeveloper とします。また、ホームディレクトリを
 /home/joedeveloper とします。(RedHat のシステムの場合そうなるでしょう)

あなたのユーザアカウントのホームディレクトリに KDE というディレクトリを
作成して下さい。これらを root 権限ではやらないで下さい! 次に、KDE/CVS-HEAD
ディレクトリを作成して下さい。そのディレクトリに移動してお好みの HEAD
ブランチから KDE のソースを取得して下さい。アプリケーションを使用するのに
必要な(最低限の)モジュールは kdesupport と kdelibs です。これらをこの順で
コンパイルしてインストールして下さい(コンパイル方法については後述します)。
その他に kdebase, kdeutils, kdegraphics などを適宜取得して下さい。

さて、KDE/kde-HEAD ディレクトリを作成して下さい。ここに Qt-2.0 と共に HEAD
ブランチからコンパイルされたコードを置きます。まず Qt-2.0 を取得しましょう
(スナップショットは ftp://ftp.troll.no/qt/snapshots にあります。最新のものを
取得しましょう。しかし、そのまま動くことを期待してはいけません。もしかしたら
上で述べた Qt スナップショットメーリングリストに何らかのヒントがあるかも
しれません)。そのソースを KDE/kde-HEAD に置きます。コンパイルするには
KDE/kde-HEAD ディレクトリに移動して次のようにします。
	gzip -d qt-2.00beta-snapshot-1999xxxx.tar.gz
	tar -xvf qt-2.00beta-snapshot-1999xxxx.tar
	ln -s qt-2.00beta-snapshot-1999xxxx qt
	cd qt
	setenv QTDIR $PWD     (csh/tcsh の場合。bash の場合は bash に合わせて変更して下さい。)
	cd configs
	mv linux-g++-shared linux-g++-shared.orig
	sed s/-fno-rtti// linux-g++-shared.orig > linux-g++-shared
	make linux-g++-shared
	make

もし sed が何をしているか知りたい場合は configs/linux-g++-shared と
configs/linux-g++-shared.orig を比較して、egcs の -fno-rtti オプションを
探してみて下さい。

さあ、KDE のコードです。KDE/CVS-HEAD/kdesupport ディレクトリに移動して
次のようにして下さい。
	make -f Makefile.cvs
	./configure --prefix=/home/joedeveloper/KDE/kde-HEAD --with-qt-dir=/home/joedeveloper/KDE/kde-HEAD/qt --with-qt-libs=/home/joedeveloper/KDE/kde-HEAD/qt/lib
	make
もしそれがうまく行ったら次のようにして下さい。
	make install
もしうまく行かなかった場合はそれらを修正してから次のようにして下さい。
	make install
他のモジュールについても同様のことを繰り返して下さい。ただし、kdelibs
モジュールをコンパイルする時は ./configure の行をつぎのように変えて下さい。
	./configure --prefix=/home/joedeveloper/KDE/kde-HEAD --with-qt-dir=/home/joedeveloper/KDE/kde-HEAD/qt --with-qt-libs=/home/joedeveloper/KDE/kde-HEAD/qt/lib --enable-new-stuff --enable-tutorials

IIIa. コンパイル tips

Qt 2.0 をコンパイルする際に -fno-rtti オプションをオフにしたのは、それが
KDE のリンクを妨げるからです。もし koffice をコンパイルするなら qimageio
(Qt の一部) を別にコンパイルする必要があるようです。
[-- 試す機会があればその方法を書く予定です。 --]

IV. KDE SDK


V. Submitting contributions

もし CVS でハックしている場合、上記の手続きにしたがってコミットして下さい。
もし CVS 以外の方法で開発している場合、あなたのコード(ウィジェット、
アプリケーションなど) を ftp://upload.kde.org/Incoming に送信して下さい。
この時、 .lsm (Linux Software Map) ファイルを添付するのを忘れないで下さい。
このようにするとあなたのコードは自動的に ftp.kde.org とそのミラーの適切な
場所に置かれ、自動的に kde-announce にアナウンスが送られます。
.lsm のサンプルは次のようになります。

Begin3
Title:          KLab
Version:        0.1.0
Entered-date:   3/1/99
Description:    GUI and more for RLab
Keywords:       kde rlab math plot plotting
Author:         David Sweet <dsweet@chaos.umd.edu>
Maintained-by:  David Sweet <dsweet@chaos.umd.edu>
Home-page:      http://www.glue.umd.edu/~dsweet/KDE/KLab
Primary-site:   ftp://ftp.kde.org/pub/kde/unstable/apps/scientific
Alternate-site: http://www.glue.umd.edu/~dsweet/KDE/KLab/
Original-site:  ftp://upload.kde.org/pub/kde/Incoming
Platform:       unix
Copying-policy: GPL
End

このテキストを mykapp.lsm にカット&ペーストして適切な変更を加えて下さい。
.lsm ファイルはソースコードとは別にアップロードしてなければなりません。
ソースコードは .tar.gz (または .tgz) あるいは .tar.bz2 にパッケージしなければ
なりません。そのアーカイブは全てのものが一つのディレクトリに展開されるように
して下さい。KDE のためのパッケージ方法のより詳しい情報は [-?-] にあります。
[-- もしなければここに HOWTO として書く予定です。 --]

Vi. 謝辞

次の方々の提案に感謝いたします。(順不同)
Roberto Alsina, Waldo Bastian, Harri Porten, Samuel Wuethrich, Daniel Naber,
Martin Konold.

----------------------------------------------------------------------------
