---
title: 活動に参加する
---

日本 KDE ユーザ会の活動を手伝って下さる方は随時募集しています。
ユーザ会の活動に参加することで、あなたも KDE ユーザにより良い環境を提供する活動の一翼を担うことが出来ます。

## 日本 KDE ユーザ会における活動

日本 KDE ユーザ会では、主に以下のような活動を行っています。必ずしも日本 KDE ユーザ会という枠組みの中で行わず、個人的に行動しても問題ないものも多いですが、必要に応じてメーリングリストなどで他の人とコミュニケーションを取るのも良いでしょう。
活動にご協力いただける方は、[kde-jp@kde.org](/community/whatiskdejp/communicate/) を購読されることをお勧めします。

KDE プロジェクトはフリーソフトウェア/オープンソースソフトウェアのコミュニティです。あなたの作成した成果物が、GPL や Creative Commons など、第三者に再配布を認めるライセンスで配布される場合がありますので、予めご了承下さい。

### KDE ソフトウェア本体の翻訳

KDE の各種ソフトウェアのインタフェースを翻訳します。詳細は[翻訳ページ](/community/getinvolved/translation/)をご覧下さい。

### ドキュメントの翻訳

[userbase.kde.org](https://userbase.kde.org/) や [techbase.kde.org](https://techbase.kde.org/) などにあるドキュメントを翻訳します。手順については、「[ページを翻訳する](https://techbase.kde.org/Translate_a_Page/ja)」を参照して下さい。

### 日本語環境特有のバグのためのパッチ開発

KDE の開発者の多くは欧米系の人々ですので、ibus や SCIM などの日本語入力の問題をはじめ、日本語環境に特有のバグは、日本人が積極的に修正していく必要があります。

### Web サイトの管理

日本 KDE ユーザ会のサイト jp.kde.org の管理運営を行います。
ソースコードは [KDE の GitLab (invent.kde.org)](https://invent.kde.org/websites/jp-kde-org) で管理しています。更新を手伝って頂ける方は、マージリクエスト (GitHub におけるプルリクエストに相当) を送って下さい。
詳細はリポジトリ内の [README](https://invent.kde.org/websites/jp-kde-org/-/blob/master/README.md) を参照して下さい。

### イベントへの参加や開催

オープンソースカンファレンスなどに参加したり、勉強会を開催するなどして、KDE の普及・広告活動を行います。

## 供託について

ユーザ会の活動に参加する方法の一つとして、自分がおこなった作業の結果をユーザ会に**供託**するという方法もあります。
供託していただいたものは、利用者の便を図るために、配布しやすい形にユーザ会がまとめます。
内容の改変はおこないません。配布しやすい形にまとめるというのは、Web サイトや ftp サイトの適切な位置に置いたり、アーカイブとしてまとめるといった行為を指します。
あなたが作業した結果を配布してもよいかどうかの問い合わせが国内企業などからあるものと思いますが、そういった問い合わせに対してユーザ会が一括して回答する窓口を設けることで、作業者がいちいち問い合わせに回答する手間を省けるようになることも期待しています。
ユーザ会は供託していただいたものに関して**何ら権利を主張しません**。
自分の作業した結果をユーザ会に供託してもよいとお考えになる方は、[deposit@kde.gr.jp](mailto:doposit@kde.gr.jp) までご連絡下さい。その際、作業の内容などを併せてお知らせください。
ユーザ会に寄せられた供託物の配布に関するお問い合わせは [info@kde.gr.jp](mailto:info@kde.gr.jp) までお願いします。

供託してくださった方は自動的に供託者メーリングリストに登録されます。
このメーリングリストには問い合わせ窓口へ寄せられたメールを転送します。
もし、供託はするが供託者メーリングリストに登録されたくないという方は供託してくださる時にその旨を書いてください。

## 本家 KDE で活動するには

本家の KDE では、各種ソフトウェアの開発に参加したり、国際レベルでの KDE の運営に関わったりできます。
最低限の英語力さえあれば、様々な分野で活動することが出来ます。
各項目のリンク先は、本家サイトのページです。

## [<img class="header-image" src="images/documentation.png" alt="Documentation" decoding="async"> ドキュメント作成](https://community.kde.org/Get_Involved/documentation)

KDE ソフトウェアは、多くの人々に利用されています。
役に立つ最新のドキュメントを提供することは、ユーザが使い方を理解する上で、非常に効果的でしょう。

## [<img class="header-image" src="images/accessibility.png" alt="Accessibility" decoding="async"> アクセシビリティ](https://community.kde.org/Get_Involved/accessibility)

KDE ソフトウェアをより多くの人々に利用して貰えるようにする仕事です。視覚や聴覚や運動障害の人々がより楽に操作できるよう、手助けをします。

## [<img class="header-image" src="images/translation.png" alt="Translation" decoding="async"> 翻訳](https://community.kde.org/Get_Involved/translation)

複数の自然言語を使えるのであれば、あなたの翻訳を KDE ソフトウェアに取り込むことで、国際市場での KDE の地位を向上させ、潜在的なユーザーにとってアクセスしやすくなるでしょう。

## [<img class="header-image" src="images/development.png" alt="Development" decoding="async"> 開発](https://community.kde.org/Get_Involved/development)

KDEコミュニティで開発者になることによって、興味深く面白い経験を楽しむ中で、大きく成長することでしょう。世界中の人々との協働を通して、新機能の実装やバグ潰し、魅力的な製品の開発を行う中で、あなたはより良い技術者となるでしょう。

## [<img class="header-image" src="images/art.png" alt="Art" decoding="async"> アートワーク](https://community.kde.org/Get_Involved/design)

印象的な画像を作成できますか? KDE コミュニティは、常に良質な画像、そして良質なイラストレータを求めています。
このような国際的な Web 中のボランティアから成るチームで働くのはきっと難しいことでしょうが、それだけのものは返ってくるはずです。
アイコンやスプラッシュスクリーン、テーマはアプリケーションのアイデンティティとなります。
KDE コミュニティに画像を提供することによって、あなたの "作品集" が多くの人々の目に触れ、又、KDE の製品に強力なブランディングをもたらし、明確に差別化することができるのです。

## [<img class="header-image" src="images/promotion.png" alt="Promotion" decoding="async"> プロモーション](https://community.kde.org/Get_Involved/promotion)

広報についての知識をお持ちですか?
KDE のマーケティングとプロモーションのほとんどは、民衆の努力によってなされています。
チームの一員となることで、KDE の恩恵を受けることができないかもしれない人々に、KDE を広めることができるでしょう。
柔軟で面白いチームの一員になって、世界を動かしましょう!

## [<img class="header-image" src="images/bugtriaging.png" alt="Bug Triaging" decoding="async"> バグの選別](https://community.kde.org/Guidelines_and_HOWTOs/Bug_triaging)

KDE BugSquad に参加することで、開発者に適切なバグを素早く知らせ、彼らのワークフローを最適化し、より少ない時間で問題を解決し、KDE コミュニティに効果的なサポートをすることができます。
Our team keeps track of incoming bugs in KDE software, and goes through old bugs.
私たちはバグの存在、再現可能性、そして報告者が充分な情報を提供しているかを確認します。
参加するためにプログラミングスキルは必要ありません。
However experience has shown us that our team members often learn so much and have so much fun we often lose them at some point to the ranks of the developer teams...
